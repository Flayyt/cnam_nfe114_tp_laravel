<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'simpleController@accueil');

Route::get('/tirage', function () {
  dump($_REQUEST);
});
Route::post('/tirage', function () {
  dump($_REQUEST);
});

Route::get('/formulaire', function () {
    return view('form'); //nom de la vue sans l'extention ".blade.php"
});

/*
 * Route permettant d'afficher la liste des utilisateurs
 */
Route::get('/users', 'simpleController@liste');

/*
 * Route permettant d'afficher le formulaire d'ajout d'un utilisateur
 */
Route::get('/users/add', 'simpleController@add');

/*
 * Route permettant de simuler l'ajout d'un utilisateur (en attendant d'avoir une bd)
 */
Route::post('/users', 'simpleController@store');


/*
 * RouteS utilisant le controller REST pour la resource users
 */
Route::resource('restusers', 'UserController');
