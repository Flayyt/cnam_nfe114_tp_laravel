<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('restusers.liste', ['users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('restusers.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors = [];
        //NB: pour ces trois champs, on compare avec NULL car le framework transforme automatiquement les chaine vides ("") en valeur null
        if( $request->name     === null )                 { $errors[] = "Le champ NOM est vide"; }
        if( $request->email    === null )                 { $errors[] = "Le champ EMAIL est vide"; }
        if( $request->password === null )                 { $errors[] = "Le champ PASSWORD est vide"; }

        if( strlen($request->password) <=8 )            { $errors[] = "Le champ PASSWORD est trop court"; }
        if( !filter_var($request->email, FILTER_VALIDATE_EMAIL)) { $errors[] = "Le champ EMAIL est invalide"; }
        if(count($errors)>0) {
          return redirect()->route('restusers.create')->with('errors', $errors);
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->save();
        return view('restusers.confirmAdd', ['user'=>$user]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('restusers.show',['id'=>$id, 'user'=>$this->users[$id]]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('restusers.edit',['id'=>$id, 'user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->save();
        return view('restusers.confirmUpdate', ['user'=>$user]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return view('restusers.confirmDelete', ['user'=>$user]);
    }
}
