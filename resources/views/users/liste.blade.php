@extends ('layouts.app')

@section ('titre', "Liste des utilisateurs")

@section ('content')
<h1>@yield('titre')</h1>
<ul>
@foreach ($users as $user)
  <li> {{ $user['name'] }} | {{ $user['email'] }} </li>
@endforeach
</ul>
@endsection
