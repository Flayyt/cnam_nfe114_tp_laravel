<!DOCTYPE html>
<html lang="fr">
  <head>
     <meta charset="utf-8" />
     <title>@yield('titre')</title>
  </head>
  <body>
@if(isset($errors) && count($errors)>0)
  @include('layouts.errors')
@endif
@yield('content')
  </body>
</html>
