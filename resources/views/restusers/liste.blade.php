@extends ('layouts.app')

@section ('titre', "Liste des utilisateurs")

@section ('content')
<h1>@yield('titre')</h1>
<ul>
@foreach ($users as $user)
  <li>
   {{ $user->name }} |
   {{ $user->email }} |
   Entreprise: {{$user->entreprise->name or ''}} |
   Groupes:
   @foreach ($user->groups as $group)
     {{$group->name}},
   @endforeach

   @include ('restusers.formDelete', ['id'=>$user->id])
   <a href="/monapplication/restusers/{{$user->id}}/edit">Modifier</a>

 </li>
@endforeach
</ul>

<a href="/monapplication/restusers/create">Ajouter</a>
@endsection
