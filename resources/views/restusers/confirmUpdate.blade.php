@extends ('layouts.app')

@section ('titre', 'Modifier un utilisateur')

@section ('content')
<h1>@yield('titre')</h1>
<p>
L'utilisateur: {{$user->name}} ({{$user->email}}) a bien été modifié !
</p>
@endsection
