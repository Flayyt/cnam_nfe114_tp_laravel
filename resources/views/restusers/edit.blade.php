@extends ('layouts.app')

@section ('titre', 'Modifier un utilisateur')

@section ('content')
<h1>@yield('titre')</h1>
@include ('restusers.formulaireOnly', ['id'=>$id, 'user'=>$user])
@endsection

